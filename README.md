Multiplayer 1.0
© 09/11/2010 Marlon Schumacher, CIRMMT/McGill University

The "Multiplayer" is a standalone application for processing/decoding/diffusion of multichannel audio files in various formats. 

Features include: 
-graphical/numerical interfaces for configuration of loudspeaker setups
-OpenGL based visualization of speaker-setup + listener (dummy head)
-ambisonic decoding & soundfield-manipulations (rotations, mirroring)
-binaural (HRTF) rendering with listener navigation
-automatic/manual compensation of time-differences and gains for non-equidistant loudspeaker setups
-one-click preset management and automatic restoring of last configuration
-all parameters are remote-controllable via OpenSoundControl/udp


The Multiplayer is implemented as a set of higher-level modules complying with the JAMOMA-Framework for MaxMSP (www.cycling74.com). Please refer to www.jamoma.org for further information & documentation. 
Jamoma is distributed under GNU Lesser General Public License (LGPL) Version 3. For more information see http://www.gnu.org/licenses/lgpl.html. 

The Multiplayer is part of the OMPrisma Library for spatial sound synthesis in the computer-aided composition environment OpenMusic6.
"Spatialisateur" is an IRCAM registered trademark. The use of this software and its documentation is restricted to members of the Ircam software users group.


DISCLAIMER:
This software is provided 'as-is', without any express or implied warranty. It has been tested on MacOS10.6.3 on Intel Mac. The PPC platform is not supported.
Please address any comments/questions/issues to marlon[at]music[dot]mcgill[dot]ca

------------------------------------------------
Installation:

-Drag'n drop "Multiplayer.app" into your /Applications folder.

Version History

************************************************
Release Version :   1.0
Release Date    :   09/11/2010
	
-changed dsp graph (/binaural-renderer now before /out-levels) see flowchart
-fileplayer: replaced filewatching with better method for auto-re-loading of overwritten files
-transparency and 'always on top' for GUI 
-new presets added
-added snaphot functionality for preset storage
-uses new ICST ambisonics decoder (2.0b9)


************************************************
Release Version :   0.8
Release Date    :   22/04/2010
	
-fileplayer: filewatch feature improved and working
-improvements in ambisonics-decoder module
-removed individual HRTFs for binaural rendering
-removed FIR-modes for nearfield filters (causing distortion due to a bug in spat.pan~ 4.2.3)
-added a 'Quickhelp' menu item displaying signal flow graph
-improved Cuescript (removed unnecessary data from presets)

************************************************
Release Version :   0.79
Release Date    :   22/04/2010
	
-miscellaneous fixes
-added DSP Status and Settings Options to the menubar

Known Issues:
-the filewatch feature is currently broken

************************************************
Release Version :   0.78
Release Date    :   20/04/2010
	
-fixed bug for listener visualization in ambimonitor
-improved OpenGL display 
	-rendering of sources 'beneath the floor'
	-improved shadows and transparency
	-efficiency improvements

Known Issues:
-after bypassing the binaural-rendering module the listener position isn't updated correctly
-the filewatch feature is currently broken

************************************************
Release Version :   0.77
Release Date    :   17/04/2010
	
-added listener for binauralization 
-added OpenGL based visualization of speaker-setup
-various bugfixes 
 
************************************************
Release Version :   0.21b
Release Date    :   25/03/2010
	
-added FIR-HRTFs for binauralization 
-default udp ports changed to 7071/7072
-miscellaneaous improvements 

************************************************
Release Version :   0.2b
Release Date    :   17/12/2009
	
-improved overall stability and performance
-splashup screen during loading
-floating window option
-all settings can be changed now while dsp is running
-jmod.ms.ambiDecode~: processing is switched off when bypassed			
-jmod.sur.ambiManipulate~: processing is switched off when bypassed
-jmod.ms.speakerDistgain~: new 'manual' mode for setting speaker-gains		
-jmod.sur.spatBinaural~: new binaural rendering Module using the HRTFs in spat4

************************************************
Release Version :   0.1b
Release Date    :   13/11/2009

-everything ;)